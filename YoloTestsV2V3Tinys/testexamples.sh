#!/bin/sh

if [ "$#" -ne 1 ]; then
	echo -n "Enter the network Architecture: "
	read ARCH
else
	ARCH=$1
fi

case $ARCH in
  D1YoloV2)
    echo -n "D1YoloV2\n"
    ./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/cat.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/city_scene.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/dog.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/dog2.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/Dog.png
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/eagle.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/food.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/giraffe.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/horses.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/kite.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/motorbike.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/person.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/scream.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/surf.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/wine.jpg
    ;;

  D1YoloV3)
    echo -n "D1YoloV3\n"
    ./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/cat.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/city_scene.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/dog.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/dog2.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/Dog.png
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/eagle.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/food.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/giraffe.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/horses.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/kite.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/motorbike.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/person.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/scream.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/surf.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/wine.jpg
    ;;

  D1YoloV2Tiny)
    echo -n "D1YoloV2Tiny\n"
    ./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/cat.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/city_scene.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/dog.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/dog2.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/Dog.png
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/eagle.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/food.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/giraffe.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/horses.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/kite.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/motorbike.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/person.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/scream.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/surf.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/wine.jpg
    ;;

  D1YoloV3Tiny)
    echo -n "D1YoloV3Tiny\n"
    ./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/cat.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/city_scene.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/dog.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/dog2.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/Dog.png
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/eagle.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/food.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/giraffe.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/horses.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/kite.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/motorbike.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/person.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/scream.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/surf.jpg
	./darknet1C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/wine.jpg
    ;;

    D2YoloV2)
    echo -n "D2YoloV2\n"
    ./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/cat.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/city_scene.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/dog.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/dog2.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/Dog.png
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/eagle.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/food.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/giraffe.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/horses.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/kite.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/motorbike.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/person.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/scream.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/surf.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2.cfg yolov2.weights data/wine.jpg
    ;;

    D2YoloV3)
    echo -n "D2YoloV3\n"
    ./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/cat.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/city_scene.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/dog.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/dog2.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/Dog.png
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/eagle.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/food.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/giraffe.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/horses.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/kite.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/motorbike.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/person.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/scream.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/surf.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/wine.jpg
    ;;

    D2YoloV2Tiny)
    echo -n "D2YoloV2Tiny\n"
    ./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/cat.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/city_scene.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/dog.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/dog2.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/Dog.png
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/eagle.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/food.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/giraffe.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/horses.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/kite.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/motorbike.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/person.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/scream.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/surf.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov2-tiny.cfg yolov2-tiny.weights data/wine.jpg
    ;;

    D2YoloV3Tiny)
    echo -n "D2YoloV3Tiny\n"
    ./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/cat.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/city_scene.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/dog.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/dog2.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/Dog.png
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/eagle.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/food.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/giraffe.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/horses.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/kite.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/motorbike.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/person.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/scream.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/surf.jpg
	./darknet2C detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/wine.jpg
    ;;

  *)
    echo -n "unknown\n"
    ;;
esac








