#!/bin/sh

## define end value ##
END=2000
x=$END
while [ $x -gt 0 ]; 
do 
  python randimg.py back$x
  echo $x
  x=$(($x-1))
done
