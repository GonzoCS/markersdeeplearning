#include "colors.inc"
#include "rand.inc"
#include "screen.inc"

#declare HMarker = 0.06;
#declare Rnd1 = seed(frame_number);
#declare vX = SRand(Rnd1);
#declare vY = SRand(Rnd1);
#declare RotX = SRand(Rnd1);
#declare RotY = SRand(Rnd1);
#declare RotZ = SRand(Rnd1);
//#declare scaleBack = rand(Rnd1)*5;
  
#declare f = 700;
#declare W = 640;
//#include "theta2.pov"

light_source { <0, 0, -1> color White }

//camera {
//  //angle 2.0*degrees(atan(W/2/f))
//  angle  61 //43 56 75
//  //right 4/3*x
//  //sky <0,1,0>
//  location <0, 0, 0>
//  look_at  <0, 0, 1>
//}

//Set_Camera_Location(<0,0,0>) // use your camera's location
//Set_Camera_Look_At(<0,0,1>) // use your camera's look at point

Set_Camera(<0,0,0>, <0,0,1>, 61)

#declare PosZ = 0.115 + 1.215*clock;
#declare maxY = 0.44 * PosZ;
#declare maxX = maxY * (4/3);
#declare rangY = maxY - HMarker;
#declare rangX = maxX - HMarker;

polygon {
  4,
  <-0.5,-0.5>, <-0.5,0.5>, <0.5,0.5>, <0.5,-0.5>//<0,0>,<0,1>,<1,1>,<1,0>
  texture {
    finish { ambient 1 diffuse 0 }
    pigment { image_map { png "tag.png"} 
              translate -0.5 } //Para centrarla en el poligono y que las rotaciones sean desde el centro
  }
  scale<0.12,0.12,0.12>
  rotate <45*RotX,45*RotY,180*RotZ>
  translate <rangX*vX,rangY*vY,PosZ>
}

#declare backgroundImage = texture {
 pigment { image_map { png concat("back",str(frame_number,0,0),".png") } }
 finish { ambient 1 diffuse 0 }
}

Screen_Plane (backgroundImage, 10000, <0,0>, <1,1>)




