from matplotlib import image
from matplotlib import pylab
import pylab as plt
import numpy as np
import sys

Z = np.random.random((480,640))   # Test data
#plt.imshow(Z, cmap='gray', interpolation='nearest')
#plt.show()

#print ('Number of arguments:', len(sys.argv), 'arguments.')
name = 'image.png'

if len(sys.argv) > 1:
    name = str(sys.argv[1])

if '.png' not in name:
    name = name + '.png'

image.imsave(name, Z, cmap='gray')
