#!/bin/bash

folder="vocmod4"
datafile="marker_voc_mod4.data"

for file in $folder/*.cfg
do
  echo "./darknetG detector train data/$datafile $file -map"
  ./darknetG detector train data/$datafile $file -map
done

