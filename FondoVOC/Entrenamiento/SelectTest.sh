#!/bin/bash
input="data/train_real.txt"
outut="data/test_real.txt"
x=1
while IFS= read -r line
do
  if [ $x -le 200 ]
  then
    text="${line/.png/.txt}"
    mv "$line" "data/realtest"
    mv "$text" "data/realtest"
    echo "$line"
    x=$(( $x + 1))
  fi
done < "$input"
