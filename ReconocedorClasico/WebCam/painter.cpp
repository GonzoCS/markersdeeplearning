#include "painter.h"
#include "escena.h"

Painter::Painter(QWidget *parent)
: QWidget(parent) {
    // setMinimumSize(500, 355);

    QPushButton *quit = new QPushButton(tr("Quit"));
    quit->setFont(QFont("Times", 14, QFont::Bold));
    connect(quit, SIGNAL(clicked()), qApp, SLOT(quit()));

    escena = new Escena(this);
    connect(escena, SIGNAL(changePos()), SLOT(newPosition()));
    connect(escena, SIGNAL(changeSize()), SLOT(newSize()));

    myinit = new QPushButton(tr("Init"));
    myinit->setFont(QFont("Times", 14, QFont::Bold));
    connect(myinit, SIGNAL(clicked()), escena, SLOT(initscreen()));

    stop = new QPushButton(tr("Stop"));
    stop->setFont(QFont("Times", 14, QFont::Bold));
    connect(stop, SIGNAL(clicked()), escena, SLOT(stop()));

    capture = new QPushButton(tr("Capture"));
    capture->setFont(QFont("Times", 14, QFont::Bold));
    connect( capture, SIGNAL(clicked()), escena, SLOT( captureHere() ));

    captureBox = new QPushButton(tr("Capture Box"));
    captureBox->setFont(QFont("Times", 14, QFont::Bold));
    connect( captureBox, SIGNAL(clicked()), escena, SLOT( captureBox() ));


    captureAll = new QPushButton(tr("Capture All"));
    captureAll->setFont(QFont("Times", 14, QFont::Bold));
    connect( captureAll, SIGNAL(clicked()), escena, SLOT( captureAll() ));

    QGridLayout *grid = new QGridLayout(); //2x2, 5 pixel border
    grid->addWidget(escena, 0, 1);
    grid->setColumnStretch(1, 10);

    messpos = new QLabel();
    messpos->setMaximumHeight(20);
    messpos->setFrameStyle(QFrame::WinPanel | QFrame::Sunken);
    // messpos->setBackgroundColor( messpos->colorGroup().base() );
    messpos->setAlignment(Qt::AlignCenter);

    messize = new QLabel();
    messize->setMaximumHeight(20);
    messize->setFrameStyle(QFrame::WinPanel | QFrame::Sunken);
    // messize->setBackgroundColor( messpos->colorGroup().base() );
    messize->setAlignment(Qt::AlignCenter);

    // Labels for the new displays...
    visorMouse = new QLabel(this);
    visorMouse->setMaximumHeight(20);
    visorMouse->setText("Mouse:");
    visorMouse->setAlignment(Qt::AlignCenter);

    // Labels for the new displays...
    visorTama = new QLabel(this);
    visorTama->setMaximumHeight(20);
    visorTama->setText("Tamaño:");
    visorTama->setAlignment(Qt::AlignCenter);

    QVBoxLayout *leftBox = new QVBoxLayout;
    grid->addLayout(leftBox, 0, 0);
    leftBox->addWidget(quit);
    leftBox->addWidget(myinit);
    leftBox->addWidget(stop);
    leftBox->addWidget(capture);
    leftBox->addWidget(captureBox);
    leftBox->addWidget(captureAll);
    leftBox->addWidget(visorMouse);
    leftBox->addWidget(messpos);
    leftBox->addWidget(visorTama);
    leftBox->addWidget(messize);

    setLayout(grid);
}

void Painter::newPosition() {
    int mx, my;

    escena->getPosition(&mx, &my);

    QString s;
    messpos->setText(s.sprintf("%d,%d", mx, my));
}

void Painter::newSize() {
    int mx, my;

    escena->getSize(&mx, &my);

    QString s;
    messize->setText(s.sprintf("%d,%d", mx, my));
}

