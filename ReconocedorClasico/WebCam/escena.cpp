#include "escena.h"

Escena::Escena(QWidget *parent)
: QGLWidget(parent)
{
	int deviceID = 1;             // 0 = open default camera
	// int deviceID = 0x2000;             // 0 = open default camera
    // int apiID = cv::CAP_ANY; 

    setFocusPolicy(Qt::StrongFocus);
    timer = new QTimer(this);
    timer->setInterval(1 / 30.0);//24.0
    connect(timer, SIGNAL(timeout()), this, SLOT(timerDone()));

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    //Initiating camera
    cap = new VideoCapture(); // open the default camera
	cap->open(deviceID);
	// cap->open(deviceID + apiID);
	
    if (!cap->isOpened()) { // check if we succeeded
        printf("Error while opening camera device %d\nTrying to open default camera\n",deviceID);
        cap->open(0);
        if (!cap->isOpened()) { // check if we succeeded
            printf("Error while opening camera device cap(1)\n");
            exit(0);
        }
    }
	// cols = cap->get(CV_CAP_PROP_FRAME_WIDTH); 
	// rows = cap->get(CV_CAP_PROP_FRAME_HEIGHT); 

    *(cap) >> frame; // get a frame from camera
    cv::cvtColor(frame, gray0, CV_BGR2GRAY);
    //cv::resize( grayFrame, gray1, Size(), 0.5, 0.5, cv::INTER_AREA );

	/**
	fprintf( stderr, "%d x %d\n", gray1.rows, gray1.cols );
	fprintf( stderr, "%d %d\n", gray1.step[0], gray1.step[1] );
	fprintf( stderr, "%d\n", gray1.elemSize() );
	**/

    IMG1 = matrix_allocate( gray0.rows, gray0.cols, sizeof(unsigned char) );
    if (!IMG1) {
        fprintf(stderr, "ERROR: No memory\n");
        exit(0);
    }

	setMinimumSize( gray0.cols, gray0.rows );
	capture = 0;
    captureLabeled = 0;
    captureEverything = 0;
    captureCounter = 0;

}

Escena::~Escena( )
{
	matrix_free( IMG1 );
}

void Escena::timerDone() {
    updateGL();
}

void Escena::dibuja_fondo()
{
	int i;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //getting image from camera
    *(cap) >> frame; // get a new frame from camera

    //Se hace el flip en gray1
    //flip(frame, frame, 0);

    cv::cvtColor(frame, gray0, CV_BGR2GRAY);

    //Preprocessing the image (or each frame in this case)
    ////////////////////NO RECORTAR A LA MITAD
    //cv::resize( grayFrame, gray0, Size(), 0.5, 0.5, cv::INTER_AREA );
    GaussianBlur(gray0, gray1, Size(7, 7), 1.5, 1.5); //Smooth filter
    threshold( gray1, gray1, 80, 255,  THRESH_BINARY );
    // bitwise_not(grayFrame, grayFrame);

    flip(gray1, gray1, 0);

    unsigned char *ptri = (unsigned char *) (gray1.data);
    unsigned char *ptro = (unsigned char *) IMG1->ptr[0];
    for (int i = 0; i < gray1.rows; i++) {
        for (int j = 0; j < gray1.cols; j++) 
				*ptro++ = *ptri++;
    }

    // use fast 4-byte alignment (default anyway) if possible
    // glPixelStorei( GL_UNPACK_ALIGNMENT, (gray1.step[0] & 3) ? 1 : 4 );

    // set length of one complete row in data (doesn't need to equal image.cols)
    glPixelStorei( GL_UNPACK_ROW_LENGTH, gray1.step[0] / gray1.elemSize() );

    // flip(frame, frame, 0);
    // Drawing color image to the QGL canvas
    // glDrawPixels(frame.cols, frame.rows, GL_BGR, GL_UNSIGNED_BYTE, grayFrame.data);

    // glDrawPixels( gray1.cols, gray1.rows, GL_LUMINANCE, GL_UNSIGNED_BYTE, gray1.data);
    glDisable(GL_DEPTH_TEST);
    glDrawPixels( gray1.cols, gray1.rows, GL_LUMINANCE, GL_UNSIGNED_BYTE, IMG1->ptr[0] );
    glEnable(GL_DEPTH_TEST);

	if ( capture == 1 ) {
		std::vector<int> compression_params { IMWRITE_PNG_COMPRESSION, 9 };
		flip(gray1, gray1, 0);
        cv::imwrite( "marco.png", gray1, compression_params );
		capture = 0;
	}
    // IMG1 is the camera frame in graytones, and can be processed after this line
	
    if( captureEverything){
        std::vector<int> compression_params { IMWRITE_PNG_COMPRESSION, 9 };
        cv::imwrite( "captured/"+std::to_string(captureCounter) + ".png", frame, compression_params );
        captureCounter++;

    }
    else if( reconoce( IMG1, dver ,border ) == 0 ) {
		glBegin( GL_LINE_LOOP );
		glColor3f( 1.0, 0.0, 0.0 );
		for( i=0; i<7; i++ )
			glVertex2d( dver[i].x, dver[i].y );
		glEnd();

        ///CAJA DEL OBJETO
        //GUARDAR EN ARCHIVO CON EL MISMO NOMBRE QUE EL DE LA IMAGEN
        //DE PREFERENCIA TENER UN CONTADOR DE IMAGENES GUARDADAS
        //GUARDAR SOLO LAS QUE TENGAN MARCADOR RECONOCIDO

        if(captureLabeled){
            glBegin( GL_LINE_LOOP );
            glColor3f( 0.0, 1.0, 0.0 );
                glVertex2d( border[0], border[1]);
                glVertex2d( border[2], border[1]);
                glVertex2d( border[2], border[3]);
                glVertex2d( border[0], border[3]);
                glVertex2d( border[0], border[1]);
            glEnd();

            //flip(gray0, gray0, 0);
            std::vector<int> compression_params { IMWRITE_PNG_COMPRESSION, 9 };
            cv::imwrite( "labeled/"+std::to_string(captureCounter) + ".png", gray0, compression_params );

            // #define AREA     4
            // #define FINY     3      (0,1)----(2,1)
            // #define FINX     2        |        | 
            // #define INICIOY  1      (0,3)----(2,3)
            // #define INICIOX  0

            xcenter = ((border[0]+border[2])/2.0)/gray1.cols;
            ycenter = ((border[1]+border[3])/2.0)/gray1.rows;
                
            width = ((border[2]-border[0]))/(float)gray1.cols;
            height = ((border[3]-border[1]))/(float)gray1.rows;

            #ifdef DEBUG
            printf("Marker detected\n");  
            printf("From: (%d , %d)  TO  (%d , %d) \n",border[0],border[1],border[2],border[3]);
            //printf("Data to write: %d , %f , %f , %f , %f  \n\n",1,xcenter,ycenter,width,height);
            #endif
            sprintf(name,"labeled/%d.txt",captureCounter);    
            pFile = fopen (name,"w");
            fprintf (pFile, "0 %7.6f %7.6f %7.6f %7.6f",xcenter,ycenter,width,height);
            fclose (pFile);

            captureCounter++;
        }
	}


}

void Escena::mouseMoveEvent(QMouseEvent * e) {
    //Not implemented
    return;
}

void Escena::keyPressEvent(QKeyEvent * e) {
    //Not implemented
    return;
}

//
// This method puts the initial coordinates each time window is resized
//

void Escena::resizeGL(int largo, int a) {
    windowWidth = largo;
    windowHeight = a;
    glViewport(0, 0, (GLsizei) largo, (GLsizei) a);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	glOrtho( 0.0, largo, 0.0, a, -1.0, 800.0 );

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    emit changeSize();
}

QSizePolicy Escena::sizePolicy() const {
    return QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

//timerDone calls   updateGL
//updateGL  calls   glDraw
//glDraw    calls   paintGL
void Escena::paintGL(void) {
    dibuja_fondo();
}

void Escena::initializeGL() {
    glClearColor(0.8, 0.8, 0.8, 0.0); // Background to a grey tone
	glLineWidth( 3.0 );
	glEnable( GL_LINE_SMOOTH );
}

void Escena::captureHere( void )
{
	capture = 1;
}

void Escena::captureBox( void )
{
    captureLabeled = !captureLabeled;
    if(captureEverything){
        captureEverything = 0;
        captureCounter = 0;
    }
    
}


void Escena::captureAll( void )
{
    captureEverything = !captureEverything;
    if(captureLabeled){
        captureLabeled = 0;
        captureCounter = 0;
    }
    
}