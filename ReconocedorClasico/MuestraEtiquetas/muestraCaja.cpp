#include "opencv2/opencv.hpp"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <stdio.h>
#include <string>
#include <cmath>
#include <unistd.h>

using namespace cv;
using namespace std;

char name[64],nameim[64],nametxt[64];
Mat image;
int clase;
float xcenter,ycenter,width, height;
int border[5];
FILE * pFile;
bool waitShow;

int main(int argc, char const *argv[]){
	if (argc < 2){
        printf("Por favor inserte el nombre de la imagen a analizar \n");
        scanf("%s",name);
    }else{
        //printf("%s\n", argv[1]);
        strcpy(name,argv[1]);
        char * p = strstr(name, ".png");
        if( p != NULL) {
            *p = 0; //marca final de la cadena, es decir borra la extension
        }
        strcpy(nameim,name);
        if (argc == 3)
            waitShow = atoi(argv[2]);
        else
            waitShow = 1;
    }

    strcat(nameim,".png");
    printf("\n\nOpening %s\n", nameim);
    image = imread(nameim); 

    if (image.empty()){ // Check for failure
        printf("Could not open or find the image %s\n",nameim);
        return -1;
    } 

    strcpy(nametxt,name);
    strcat(nametxt,".txt");    
    pFile = fopen (nametxt,"r");

    // #define AREA     4
    // #define FINY     3      (0,1)----(2,1)
    // #define FINX     2        |        | 
    // #define INICIOY  1      (0,3)----(2,3)
    // #define INICIOX  0

    //xcenter = ((border[0]+border[2])/2.0)/gray1.cols;
    //ycenter = ((border[1]+border[3])/2.0)/gray1.rows;
    //width = ((border[2]-border[0]))/(float)gray1.cols;
    //height = ((border[3]-border[1]))/(float)gray1.rows;
    int read = 0;
    do{
        read = 0;
        read += fscanf(pFile,"%d",&clase);
        read += fscanf(pFile,"%f",&xcenter);
        read += fscanf(pFile,"%f",&ycenter);
        read += fscanf(pFile,"%f",&width);
        read += fscanf(pFile,"%f",&height);
        
        border[0] = (int)roundf((xcenter - width/2.0)*image.cols);
        border[1] = (int)roundf((ycenter - height/2.0)*image.rows);
        border[2] = (int)roundf((xcenter + width/2.0)*image.cols);
        border[3] = (int)roundf((ycenter + height/2.0)*image.rows);

        if (read == 5){
            printf("Marker detected\n");
            printf("Read data: %d , %f , %f , %f , %f  \n",clase,xcenter,ycenter,width,height);
            printf("From: (%d , %d)  TO  (%d , %d) \n",border[0],border[1],border[2],border[3]);

            Point pt1 = Point(border[0],border[1]);
            Point pt2 = Point(border[2],border[3]);
            rectangle(image,pt1,pt2,Scalar(0,255,0),2,8,0);
        }
    }while(read == 5);
    fclose (pFile);


    
    String windowName = String(name); //Name of the window
    namedWindow(windowName); // Create a window
    imshow(windowName, image); // Show our image inside the created window.
    waitKey(0); // Wait for any keystroke in the window
    destroyWindow(windowName); //destroy the created window

    std::vector<int> compression_params { IMWRITE_PNG_COMPRESSION, 9 };
    cv::imwrite( String(name) + "Yolo.png", image, compression_params );

	return 0;
}