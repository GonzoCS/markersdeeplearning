#include <stdio.h>
#include <math.h>
#include "matrix.h"
#include "triangles.h"
#include "follow.h"
#include "lambdaMatrix.h"
#include "fill.h"

#define ESQUINAS 7
#define CENTROY  6
#define CENTROX  5
#define AREA     4
#define FINY     3
#define FINX     2
#define INICIOY  1
#define INICIOX  0

/** 
	Dilation of an object
	The resul is save in the same input image
	backgroud = tono
	object = 255
**/
void dilation_4neig_inplace_obj( MATRIX *I, int *v, byte tono )
{
	int i, j;
	byte *ptrin;
	byte **pimg;
	int val;

	pimg = (byte **) I->ptr;
	for( i=v[INICIOY]; i<=v[FINY]; i++ ) {
		ptrin = (byte *) I->ptr[i] + v[INICIOX];
		for( j=v[INICIOX]; j<=v[FINX]; j++ ) {
			if ( *ptrin == tono ) {//Pixel con fondo
				val = 0;
				if ( pimg[i-1][j] == 255 ) val = 255; //Si hay algun pixel vecino 4vec que sea del objeto
				if ( pimg[i][j-1] == 255 ) val = 255; 
				if ( pimg[i][j+1] == 255 ) val = 255; 
				if ( pimg[i+1][j] == 255 ) val = 255; 

				if ( val == 255 ) //Si por lo menos se dio uno de los anteriores
					pimg[i][j] = 254;//Marcar como que fue dilatado
			}
			ptrin++;
		}
	}
}

void quita_tiritas( MATRIX *I, int *v, byte tono )
{
	int i, j, k, l;
	byte *ptrin, *ptraux;
	int suma;

	for( i=v[INICIOY]+1; i<v[FINY]; i++ ) {
		ptrin = (byte *) I->ptr[i] + v[INICIOX] + 1;
		for( j=v[INICIOX]+1; j<v[FINX]; j++ ) {
			if ( *ptrin == tono ) {
				suma = 0;
				for ( k = -1 ; k <= 1; k++ ) {
					ptraux = (unsigned char *) I->ptr[ i+k ];
					ptraux +=  j - 1;
					for ( l = 0 ; l < 3; l++ ){
						suma += *ptraux++;
					}
				}
				suma -= 2*tono;
				if ( suma == 1785 || suma == 1530 ) // Solo tiene uno vecino. Se borra
					*ptrin = 255;
			}
			ptrin++;
		}
	}
}

int extrae_perimetro( MATRIX * IMG, int *vals, POINT *pts )
{
	int i, j, x, y, n;
	int x2;
	byte *pimg;

	x = y = -1;
	//Encuentra primer pixel del perimetro
	for( i=vals[INICIOY]; i<=vals[FINY]; i++ ) {
		pimg = (byte *)IMG->ptr[i] + vals[INICIOX];
		for( j=vals[INICIOX]; j<=vals[FINX]; j++ ) {
			if ( *pimg == 254 ) {
				x = j;
				y = i;
				goto SIGUE;
			}
			pimg++;
		}
	}
	SIGUE:
	if ( x == -1 ) {
		#ifdef DEBUG
		fprintf( stderr, "No hay puntos de perímetro\n" );
		#endif
		return -3;
	}
	/** Buscamos en el tramo "de arriba" **/
	x2 = x - 1;
	pimg = (byte *)IMG->ptr[y+1] + x2;
	if( *pimg == 254 ) {
		while ( *pimg == 254 ) {
			x2--;
			pimg--;
		}
		x2++;
	}
	if( x-x2 > 5 ) {
		pimg = (byte *)IMG->ptr[y] + x;
		while ( *pimg == 254 ) {
			x++;
			pimg++;
		}
		x--;
	}
	n = follow_perimeter( IMG, y, x, 254, pts );
	if( n > 3000 ) {
		fprintf( stderr, "Hay más de 3000 puntos en el perímetro\n" );
		fprintf( stderr, "n = %d\n", n );
		return -4;
	}
	return n;
}


int checa_linea( POINT *pts, int index1, int index2 )
{
	int i, dx, dy;
	int distance, indice, val;
	POINT pw, pv;
	double dd;

	if ( index1 >= index2 )
		return -1;

	//p2 - p1
	dx = pts[index2].x - pts[index1].x;
	dy = pts[index2].y - pts[index1].y;
	//ecuacion de la recta que pasa por el origen y por el punto restandole el primero
	pw.x = -dy;
	pw.y = dx;

	distance = 0;
	for( i=index1; i<=index2; i++ ){
		//pi - p1
		pv.x = pts[i].x - pts[index1].x;
		pv.y = pts[i].y - pts[index1].y;

		//Distancia sin la division, no afecta para encontrar el maximo
		val = abs( pv.x * pw.x + pv.y * pw.y );
		if ( val > distance ){
			distance = val;
			indice = i;
		}
	}
	//Distancia maxima real al aplicarle la division
	dd = (double)distance/sqrt( pw.x * pw.x + pw.y * pw.y );
	// printf( "# d= %lf %lf\n", dd, sqrt( dx * dx + dy * dy )/20.0 );
	// if( dd > sqrt( dx * dx + dy * dy )/75.0 )
	// if( dd > sqrt( dx * dx + dy * dy )/40.0 )
	if( dd > sqrt( dx * dx + dy * dy )/20.0 )//Criterio de distancia maxima respecto de la disancia del p1 a p2, pues se buscan cuadrados no tan deformados
		return( indice ); 
	else 
		return -2;
}


int analiza_perimetro_cuadrado( POINT *pts, int n, DPOINT *pdver )
{
	int i, i1, i2, i3, i4, imiddle;
	int flag1, flag2;
	int d2, distancia;
	DPOINT pmedio[4], vec[4];
	POINT pu;

	// El primer vértice por definición es el 0
	i1 = 0;
	// Buscamos el segundo vértice
	distancia = 0;
	i2 = 0;
	for( i=1; i<n; i++ ) {
		pu.x = pts[i].x - pts[i1].x;
		pu.y = pts[i].y - pts[i1].y;
		d2 = pu.x * pu.x + pu.y * pu.y;
		if( d2 > distancia ) {
			distancia = d2;
			i2 = i;
		}
	}

	// El segundo vértice está en el índice i2
	// Checamos en que caso estamos
	flag1 = flag2 = 0;
	// Para el caso 1, algunos de las banderas seguirá en 1
	// Para el caso 2, las dos banderas deben de valer 0
	// Prueba 1: De i1 a i2
	// Prueba 2: De i2+1 a n-1
	
	// Prueba 1:
	i3 = checa_linea( pts, i1, i2-1 );
	if( i3 == -1 )
		return -10;
	else if ( i3 < -1 )
		flag1 = 1;//El vertice 3 esta muy cerca a la diagonal

	// Prueba 2:
	i4 = checa_linea( pts, i2, n-1 );
	if( i4 == -1  )
		return -20;
	else if ( i4 < -1 )
		flag2 = 1;//El vertice 4 esta muy cerca a la diagonal

	// printf("# flags: %d %d\n", flag1, flag2 );
	
	if ( flag1 == 1 || flag2 == 1 ) { // Caso 1
		// printf("# caso 1\n" );
		if( flag2 == 1 ) {
			imiddle = i2/2;
			i4 = i2;
			i3 = checa_linea( pts, imiddle, i4 );
			i2 = checa_linea( pts, i1, imiddle-1 );
		}
		else {
			// i2 + 1 + n - 1 = i2 + n
			imiddle = ( i2+n )/2;
			i3 = checa_linea( pts, i2, imiddle-1 );
			i4 = checa_linea( pts, imiddle, n-1 );
		}

		// Buscamos el punto más alejado de la linea i2 e imiddle.

		// If flag1 = 0 A fuerzas i1 < i2 < i3 < i4
		// If flag2 = 0 A fuerzas i1 < i2 < i3 < i4
	}
	else {  // Caso 2. Ya está resuelto
		pu.x = i3;
		i3 = i2;
		i2 = pu.x;
		// Ahora afuerzas tenemos i1 < i2 < i3 < i4
	}
	// printf( "# i1: %d %d %d\n", i1, pts[i1].x, pts[i1].y );
	// printf( "# i2: %d %d %d\n", i2, pts[i2].x, pts[i2].y );
	// printf( "# i3: %d %d %d\n", i3, pts[i3].x, pts[i3].y );
	// printf( "# i4: %d %d %d\n", i4, pts[i4].x, pts[i4].y );
	// Aquí se puede verificar si es un cuadrado.
	// Las líneas que unen los cuatro vértices deben ser líneas
	// ¿Entonces un punto que está lejos un valor "umbral" de una
	// línea, hace esa línea inválida?
	// Si, lo intentamos;
	if ( checa_linea( pts, i1, i2-1 ) > 0 ){
		return -1;
	}
	if ( checa_linea( pts, i2, i3-1 ) > 0 ){
		return -2; 
	}
	if ( checa_linea( pts, i3, i4-1 ) > 0 ){
		return -3; 
	}
	if ( checa_linea( pts, i4, n-1 ) > 0 ){
		return -4; 
	}

	/**
	printf( "# i1: %d %d\n", pts[i1].x, pts[i1].y );
	printf( "# i2: %d %d\n", pts[i2].x, pts[i2].y );
	printf( "# i3: %d %d\n", pts[i3].x, pts[i3].y );
	printf( "# i4: %d %d\n", pts[i4].x, pts[i4].y );
	**/

	getVector( pts, i1, i2-1, &pmedio[0], &vec[0] );
	getVector( pts, i2, i3-1, &pmedio[1], &vec[1] );
	getVector( pts, i3, i4-1, &pmedio[2], &vec[2] );
	getVector( pts, i4, n-1,  &pmedio[3], &vec[3] );

	intersecs( &pmedio[3], &vec[3], &pmedio[0], &vec[0], &pdver[0] );
	intersecs( &pmedio[0], &vec[0], &pmedio[1], &vec[1], &pdver[1] );
	intersecs( &pmedio[1], &vec[1], &pmedio[2], &vec[2], &pdver[2] );
	intersecs( &pmedio[2], &vec[2], &pmedio[3], &vec[3], &pdver[3] );

	/**
	printf( "# %lf %lf\n", pdver[0].x, pdver[0].y );
	printf( "# %lf %lf\n", pdver[1].x, pdver[1].y );
	printf( "# %lf %lf\n", pdver[2].x, pdver[2].y );
	printf( "# %lf %lf\n", pdver[3].x, pdver[3].y );
	**/
	#ifdef DEBUG
	printf("Cuadrado!\n");
	#endif
	return 0;
}

/** 
  Pone alrededor de la imagen, un borde blanco (255) 
  del ancho de 1 pixel
**/ 
void limpia_borde( MATRIX *I )
{
	unsigned int i;
	byte *paux;

	paux = (byte *)I->ptr[0];
	for( i=0; i<I->cols; i++ )
		*paux++ = 255;

	for( i=1; i<I->rows-1; i++ ) {
		paux = (byte *)I->ptr[i];
		*paux = 255;
		paux += I->cols - 1;
		*paux = 255;
	}

	paux = (byte *)I->ptr[ I->rows - 1 ];
	for( i=0; i<I->cols; i++ )
		*paux++ = 255;
}

//Busca un punto del objeto
int find_point_local_object( MATRIX *IN, int *v, int *x, int *y )
{
	int i, j;
	byte *p;

	for ( i=v[INICIOY]; i<= v[FINY]; i++ ) {
		p = (byte *)IN->ptr[i] + v[INICIOX];
		for ( j=v[INICIOX]; j<=v[FINX]; j++ ) {
			if ( *p == 0) {
				*x = j;
				*y = i;
				return 0; /** Sucess! **/
			}
			p++;
		}
	}
	return 1; /** Fault **/
}


int reconoce( MATRIX * IMG, DPOINT *retver  ,int* extbord)
{
	int i, j, n, p;
	int tonoObjeto=1;
    POINT iver[7];
    DPOINT dver[7];
    int indexes[7];
    POINT ver[4];
	static int state=0;
	static int border[5];
    static POINT per1[3000];

	limpia_borde( IMG );
	quita_tiritas( IMG, border, 0 );

	#ifdef DEBUG
	printf("\nNueva %d\n", state );
	#endif
	while(1) {//Loop inifinito
		if( state == 0 ){
			// Encuentra un punto de un objeto en la imagen entera. Es negro( 0 )
			border[INICIOX] = 1;
			border[INICIOY] = 1;
			border[FINX] = IMG->cols-2;
			border[FINY] = IMG->rows-2;
			p = find_point_local_object( IMG, border, &j, &i);
		}
		else if( state == 1 ){//Encontro un marcador
			#ifdef DEBUG
			printf("%d %d %d %d\n", border[INICIOX], border[INICIOY], border[FINX], border[FINY] );
			#endif
			p = find_point_local_object( IMG, border, &j, &i);
		}

		//printf("find_point_object --> j=%d, i=%d\n",j,i);
		if ( p ) {
			// No hay un cuadrado en la escena
            state = 0;
            return 1;
		}

		//Si hay objeto busca el pixel mas a la izquierda del renglon
		p = left ( IMG, j, i );
		//Guarda el primer pixel del renglon en una pila
		push( p, i );
		//Rellena el objeto y actualiza los bordes de acurdo a lo guardado en la pila
		do_fill( IMG, border, tonoObjeto );

		if ( border[INICIOX] == 1 || border[INICIOY] == 1 ||
		     border[FINX] == (int)IMG->cols-2 || border[FINY] == (int)IMG->rows-2 ) {
			tonoObjeto++;
			continue;
		}

		// Calcular el perímetro
		dilation_4neig_inplace_obj( IMG, border, tonoObjeto );

		// Perimeter is now with values 254
		n = extrae_perimetro( IMG, border, per1 );
		#ifdef DEBUG
		printf("E1\n");
		#endif
		if ( n > 25 ) {
			if ( analiza_perimetro_cuadrado( per1, n, dver ) == 0 ){
				state = 1;
				break;
			}
			else {
				tonoObjeto++;
				continue;
			}
		}
	}
	clean_stack();

	// printf("\n\n");
	// Triangle's perimeter
	n = extrae_perimetro( IMG, border, per1 );
	#ifdef DEBUG
	printf("E2\n");
	#endif
	if ( n > 20 ) {
		if( extrae_triangulo( per1, n, ver, &dver[4] ) < 0 ) {
			fprintf( stderr, "Mal triangulo\n");
			return(2);
		}
	}
	#ifdef DEBUG
	printf( "Triangulo %d!\n", state );
	#endif

	// Here I have all the 7 points of the marker
	for( i=0; i<7; i++ ){
		iver[i].x = (int)( dver[i].x + 0.5 );
		iver[i].y = (int)( dver[i].y + 0.5 );
		// Image is already inverted
		// iver[i].y = (int)( IMG->rows - 1 - dver[i].y + 0.5 );
	}
	lambdaMatrix( iver, indexes );
	for(i=0; i<7; i++) {
		retver[i].x = dver[ indexes[i] ].x;
		retver[i].y = dver[ indexes[i] ].y;
		// retver[i].y = IMG->rows - 1 - dver[ indexes[i] ].y;
		// printf("%lf %lf\n", retver[i].x, retver[i].y );
	} 


	//EN ESTE PUNTO YA SE SABE QUE SE ENCONTRO UN MARCADOR Y ESTA DENTRO DE border
	extbord[0] = border[0];
	extbord[1] = border[1];
	extbord[2] = border[2];
	extbord[3] = border[3];
	extbord[4] = border[4];

	return( 0 );
}

