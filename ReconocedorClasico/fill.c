#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"
#include "fill.h"


#define BACKTONE	255
#define OBJTONE		0

#define UP   	0
#define DOWN	1
#define E1  	2
#define E2  	3
#define NPARAM 	4

/** Variables for STACK **/

typedef int typexy;
typedef unsigned char uchar;

#define STACK_SIZE 3000

POS stack[ STACK_SIZE ];
int pos_stack = -1; 

/********** Funtiones for the stack ******************/
int push( int a, int b )
{
	int pone;
 
	pone = pos_stack;
	pone++;
	if ( pone >= STACK_SIZE ){
		fprintf( stderr, "Stack is Full %d!\n", pone );
		exit(1);
	}
	stack[pone].a = a;
	stack[pone].b = b;
	pos_stack = pone;
	return( 1 );      /** ok! **/
}

int pop( int *a, int *b )
{
	if ( pos_stack<0 ) return(0);   /** Error **/
	*a = stack[pos_stack].a;
	*b = stack[pos_stack].b;
	pos_stack--;
	return( 1 );    /** Ok! **/
}

int empty_stack( void )
{
	if( pos_stack<0 ) return( 0 ); /** It's empty **/
	return( 1 );                    /** There is something **/
}

void clean_stack( void )
{
	pos_stack = -1;
}

//Rellena fila con tono y devuelve coordenada de ultimo llenados
int fill ( MATRIX *IMG, int x, int y, byte tono )
{
	byte *pimg;

	pimg = (byte *)IMG->ptr[y] + x;
	while ( *pimg == OBJTONE ) {
		*pimg++ = tono;
		x++;
	}
	return( --x );
}

//Dado un pixel recorre la imagen a la izquierda mientras se encuentre sobre el objeto y devuelce la coordenada x mas a la izquierda
int left ( MATRIX *IMG, int x, int y )
{
	byte *pimg;

	pimg = (byte *)IMG->ptr[y] + x;
	while ( *pimg == OBJTONE ) {
		--x;
		pimg--;
	}
	return ( ++x );
}

int link( MATRIX *IMG, int x, int y, int *par  )
{	
	//Contadore de cuantos puntos hay arriba y abajo a la iquierda
	int above=0, below=0;
	//inicio de los renglones arriba y abajo
	int e1, e2 = -1;
	byte **img;

	img = (byte **)IMG->ptr;

	if ( img[y-1][x-1]==OBJTONE ) {
		above++;
		e1 = left( IMG, x-1, y-1 );
	}
	if ( img[y+1][x-1]==OBJTONE ) {
		below++;
		e2 = left( IMG, x-1, y+1 );
	}
	//Moverse a la derecha mientras siga sobre el objeto
	while ( img[y][x] == OBJTONE ) {
		//Esquina superior izquierda es fondo y pixel de arriba es objeto
		if ( img[y-1][x-1]==BACKTONE && img[y-1][x]==OBJTONE ) {
			if ( above ) push ( e1, y-1 );
			above++;
			e1 = left( IMG, x, y-1 );
		}
		//Esquina inferior izquierda es fondo y pixel de abajo es objeto
		if ( img[y+1][x-1]==BACKTONE && img[y+1][x]==OBJTONE ) {
			if ( below ) push ( e2, y+1 );
			below++;
			e2 = left( IMG, x, y+1 );
		}
		x++;
	}
	if ( img[y-1][x-1]==BACKTONE && img[y-1][x]==OBJTONE ){
		if ( above ) push ( e1, y-1 );
		above++;
		e1 = left( IMG, x, y-1 );
	}
	if ( img[y+1][x-1]==BACKTONE && img[y+1][x]==OBJTONE ){
		if ( below ) push ( e2, y+1 );
		below++;
		e2 = left( IMG, x, y+1 );
	}
	par[UP] = above;
	par[DOWN] = below;
	par[E1] = e1;
	par[E2] = e2;
	return( 1 );
}

//Rellena objeto de acuerdo a lo quehay en la pila y devuelve sus extremos en *val
int do_fill( MATRIX *IMG, int *val, byte tono )
{
	int x, y;
	int param[ NPARAM ];
	int pnext, dir;
	//Extremos de busqueda
	int p1x, p1y, p2x, p2y, v;
	int area = 0;
	byte **img;

	//Inicializa extremos con maximos y minimos
	p1x = IMG->cols;
    p2x = 0;

	p1y =IMG->rows;
	p2y = 0;

	img = (byte **)IMG->ptr;
	dir = UP;
	while( pop( &x, &y ) ){//Mientras haya puntos en la pila (El primer punto debio ser el encontrado en reconoce())

		//Actualiza p12xy con valores de pixel actual
		if ( x < p1x ) p1x = x;
		else if ( x > p2x ) p2x = x;
		if ( y < p1y ) p1y = y;
		else if ( y > p2y ) p2y = y;

		for(;;){//Loop infinito
			if ( !link( IMG, x, y, param ) ) return(0);
			if ( !param[UP] && !param[DOWN] ){//Si no tien puntos ni arriba ni abajo Detiene loop infinito
				if ( img[y][x]==OBJTONE ) {//Si es un punto color del objeto (negro)
					v = fill( IMG, x, y, tono );//Rellenar con el tono y devuelve ultima columna
					area += v - x + 1;     //Suma area de lo rellenado
					if ( v > p2x ) p2x = v;//Acualiza limite derecho
				}
				break;//Detiene loop Infinito
			}

			if ( !param[UP] ) dir=DOWN; //Si no tiene puntos arriba (Pero si abajo)
			else if ( !param[DOWN] ) dir=UP;//Si no tiene puntos abajo (Pero si arriba)

			if ( param[UP] && param[DOWN] ) { //Si tiene puntos arriba y abajo
				if( dir==UP ) push ( param[ E1+DOWN ], y+1 );
				else  push ( param[ E1+UP ], y-1 );
			}
			//Actualiza x siguiente segun direccion
			pnext = param[ E1+dir ];
			//Rellena fila hacia la derecha
			v = fill( IMG, x, y, tono );
			area += v - x + 1;
			if ( v > p2x ) p2x = v;

			//Actualiza x,y de acuerdo a direccion
			x = pnext;
			y = (dir==UP) ? y-1 : y+1;

			//Actualiza extremos de busqueda
			if ( x < p1x ) p1x = x;
			else if ( x > p2x ) p2x = x;
			if ( y < p1y ) p1y = y;
			else if ( y > p2y ) p2y = y;

			// if ( y<0 || y==IMG->rows ) return(0);
		}
	}
	val[INICIOX] = p1x;
	val[INICIOY] = p1y;
	val[FINX] = p2x;
	val[FINY] = p2y;
	val[AREA] = area;
	return( 0 );
} 
