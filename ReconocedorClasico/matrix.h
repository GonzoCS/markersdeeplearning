#ifndef MATRIXS_H
#define MATRIXS_H 1

#include <stdio.h>
#include <stdlib.h>
// #include <malloc.h>
#include <string.h>

typedef unsigned char byte;

typedef struct {
    int element_size;
    int type;
    unsigned int rows;
    unsigned int cols;
    void **ptr;
} MATRIX;

typedef struct {
    byte R, G, B;
} RGB;


#define ESQUINAS 7
#define CENTROY 6
#define CENTROX 5
#define AREA    4
#define LARGO   3
#define ANCHO   2
#define INICIOY 1
#define INICIOX 0
/** 
    type = 0   1 bit/pixel, 0 is write
    type = 1   1 bit/pixel, 1 is write 
    type = 2   4 bits/pixel, 16 grey tones
    type = 3   4 bits/pixel, palette 16 colors
    type = 4   8 bits/pixel, palette 256 colors 
    type = 5   8 bits/pixel, 256 grey tones
    type = 6   24 bits/pixel, RGB
    type = 7   16 bits/pixel, short integers matrix 
    type = 8   32 bits/pixel, integers matrix 
    type = 9   32 bits/pixel, floats matrix 
    type = 10  64 bits/pixel, doubles matrix 
 **/
#define pixels2bytes(n)     (((n)+7)/8)
#define greyvalue(r,g,b)  (((r*30)/100) + ((g*59)/100) + ((b*11)/100))

//void strmfe(char *new, char *old, char *ext);

MATRIX *matrix_allocate(unsigned int rows, unsigned int cols, int element_size) ;

void matrix_free(MATRIX *A) ;
int comparaRGB(RGB color1, RGB color2);

#endif
