#include "imagenReconoce.h"


int main(int argc, char const *argv[]){
	if (argc < 2){
		printf("Por favor inserte el nombre de la imagen a analizar \n");
		scanf("%s",name);
	}else{
		//printf("%s\n", argv[1]);
		strcpy(name,argv[1]);
		char * p = strstr(name, ".png");
		if( p != NULL) {
    		*p = 0; //marca final de la cadena, es decir borra la extension
		}
		strcpy(nameim,name);
		if (argc == 3)
			waitShow = atoi(argv[2]);
		else
			waitShow = 1;
	}

	strcat(nameim,".png");
	printf("Opening %s\n", nameim);
	image = imread(nameim); 

	if (image.empty()){ // Check for failure
		printf("Could not open or find the image %s\n",nameim);
		return -1;
	}

	cv::cvtColor(image, gray0, CV_BGR2GRAY);

	IMG1 = matrix_allocate( gray0.rows, gray0.cols, sizeof(unsigned char) );
    if (!IMG1) {
        fprintf(stderr, "ERROR: No memory\n");
        exit(0);
    }

 
	//Preprocessing the image (or each frame in this case)
    GaussianBlur(gray0, gray1, Size(7, 7), 1.5, 1.5); //Smooth filter
    threshold( gray1, gray1, 80, 255,  THRESH_BINARY );

    /////////////////////////SE DEBE VOLTEAR???
    //flip(gray1, gray1, 0);

    unsigned char *ptri = (unsigned char *) (gray1.data);
    unsigned char *ptro = (unsigned char *) IMG1->ptr[0];
    for (int i = 0; i < gray1.rows; i++) {
        for (int j = 0; j < gray1.cols; j++) 
				*ptro++ = *ptri++;
    }


    if( reconoce( IMG1, dver ,border ) == 0 ) {

    	std::vector<int> compression_params { IMWRITE_PNG_COMPRESSION, 9 };
        // #define AREA     4
        // #define FINY     3      (0,1)----(2,1)
        // #define FINX     2        |        | 
        // #define INICIOY  1      (0,3)----(2,3)
        // #define INICIOX  0
        xcenter = ((border[0]+border[2])/2.0)/gray1.cols;
        ycenter = ((border[1]+border[3])/2.0)/gray1.rows;
            
        width = ((border[2]-border[0]))/(float)gray1.cols;
        height = ((border[3]-border[1]))/(float)gray1.rows;

        //printf("Marker detected\n");  
        printf("Detected from: (%d , %d)  to  (%d , %d) \n",border[0],border[1],border[2],border[3]);
        printf("Data to write: %d , %f , %f , %f , %f  \n\n",0,xcenter,ycenter,width,height);

        strcpy(nametxt,name);
        strcat(nametxt,".txt");    
        pFile = fopen (nametxt,"w");
        fprintf (pFile, "0 %7.6f %7.6f %7.6f %7.6f",xcenter,ycenter,width,height);
        fclose (pFile);

        if(waitShow){
        	//SHOW RESULT
	        //void rectangle(Mat& img, Point pt1, Point pt2, const Scalar& color, int thickness=1, int lineType=8, int shift=0)
	        Point pt1 = Point(border[0],border[1]);
	        Point pt2 = Point(border[2],border[3]);
	        rectangle(image,pt1,pt2,Scalar(0,255,0),2,8,0);

	        String windowName = "Detected"; //Name of the window
			namedWindow(windowName); // Create a window
			imshow(windowName, image); // Show our image inside the created window.
			waitKey(0); // Wait for any keystroke in the window
			destroyWindow(windowName); //destroy the created window
		}
    }else{
    	printf("Marker not detected in %s\n\n",nameim);
    } 

	return 0;
}