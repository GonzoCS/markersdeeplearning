#include "opencv2/opencv.hpp"

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include <stdio.h>
#include <string.h>
#include <unistd.h>
//Includes C code files
extern "C" {
#include "../matrix.h"
#include "../triangles.h"
#include "../reconoce.h"
}
using namespace cv;
using namespace std;

char name[64],nameim[64],nametxt[64];
//std::string name,nameim,nametxt;
Mat image;
MATRIX *IMG1;
Mat grayFrame,gray0, gray1;
DPOINT dver[7];
float xcenter,ycenter,width, height;
int border[5];
FILE * pFile;
bool waitShow;
