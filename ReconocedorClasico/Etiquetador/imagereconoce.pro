######################################################################
# Automatically generated by qmake (1.07a) Wed Oct 6 11:42:48 2004
######################################################################

TEMPLATE = app
TARGET		= recoview
INCLUDEPATH += .

###COMO VENIA
############################################
#INCLUDEPATH += "/usr/local/opt/opencv@3/include"
#LIBS += "-L/usr/local/opt/opencv@3/lib" "-L/usr/local/lib" -lopencv_imgproc -lopencv_core -lopencv_videoio -lopencv_imgcodecs
############################################

###PARA MI MAQUINA
#############################################
INCLUDEPATH += "/usr/local/include/opencv2"
LIBS += "-L/usr/include/opencv2" -lopencv_imgproc -lopencv_core -lopencv_videoio -lopencv_imgcodecs `pkg-config opencv --cflags --libs`
#############################################

###QT += opengl

# Input
HEADERS += imagenReconoce.h ../matrix.h \
  ../follow.h ../lambdaMatrix.h ../triangles.h ../fill.h ../reconoce.h
SOURCES += imagenReconoce.cpp ../matrix.c ../follow.c \
  ../lambdaMatrix.c ../reconoce.c ../triangles.c ../fill.c

#DEFINES += DEBUG
