#ifndef FOLLOW_H
#define FOLLOW_H 1

#include "matrix.h"
#include "triangles.h"

int follow_perimeter( MATRIX *, int, int, byte, POINT * );

#endif
