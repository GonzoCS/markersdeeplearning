#ifndef FILL_H
#define FILL_H 1
 
#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"


#define BACKTONE	255
#define OBJTONE		0
#define FILLTONE	10

#define UP   	0
#define DOWN	1
#define E1  	2
#define E2  	3
#define NPARAM 	4

/** Variables for STACK **/

typedef int typexy;
typedef unsigned char uchar;

#define STACK_SIZE 3000

typedef struct {
	typexy a, b;
} POS;

//extern POS stack[ STACK_SIZE ];
//extern int pos_stack = -1; 

/********** Funtiones for the stack ******************/
int push( int , int  );
int pop( int *, int * );
int empty_stack( void );
void clean_stack( void );
int fill ( MATRIX *, int, int, byte );
int left ( MATRIX *, int , int  ) ;
int link( MATRIX *, int , int , int *  );

#define ESQUINAS 7
#define CENTROY  6
#define CENTROX  5
#define AREA     4
#define FINY     3
#define FINX     2
#define INICIOY  1
#define INICIOX  0

int do_fill( MATRIX *, int *, byte );

#endif
