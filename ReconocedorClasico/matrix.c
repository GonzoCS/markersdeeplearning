#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "matrix.h"

void strmfe ( char *new, char *old, char *ext )
{
	while ( *old != 0 && *old != '.' ) *new++ = *old++;
	*new++ = '.';
	while ( *ext ) *new++ = *ext++;
	*new = 0;
}


MATRIX *matrix_allocate (unsigned int rows, unsigned int cols, int element_size)
{
	int i;
	MATRIX *A;
	char **paux;

	/** Allocate the matrix structure **/
	A = (MATRIX *) malloc ( sizeof( MATRIX ) );
	if ( !A ) {
		fprintf( stderr, "\nERROR en colocar la estructura de la matriz\n");
		exit (1);
	}
	/** Set size as requiered **/
	A->rows = rows;
	A->cols = cols;
	A->element_size = element_size;

/** Try to allocate the request **/
	switch ( element_size ) {

		case  sizeof( byte ): 
			A->type = 5;
			break;
		case  sizeof( RGB ): 
			A->type = 6;
			break;
		case  sizeof( float ): 
			A->type = 9;
			break;
		case  sizeof( double ): 
			A->type = 10;
			break;
		default:
			fprintf( stderr, "\nERROR en localizar matriz: tipo no soportado\n");
			exit(1);
	}
	paux = ( char **) malloc ( rows * sizeof( char *) );
	if ( !paux ) {
		fprintf( stderr, "\nERROR haciendo apuntadores en %dx%d matrix de caracteres\n", rows, cols );
		exit(1);
	}
	paux[0] = (char *) malloc( cols * rows * element_size );
	if( !paux[0] ) {
		fprintf( stderr, "\nError haciendo renglón 0 en %dx%d  matriz de caracteres\n", rows, cols );
		exit(1);
	}
	for ( i=1; i<rows; i++ ) {
		paux[i] = paux[i-1] + cols*element_size; 
	}
	A->ptr = (void ** ) paux;

	return( A );
}


void matrix_free( MATRIX *A )
{
	// int i;
	char **a;

	a = (char **)A->ptr;
	free( a[0] );
	/** for ( i=0; i<A->rows; i++ ) free( a[i] ); **/
	free ( a );
	free ( A );
}


/**
   Compara dos variables RGB. Si son iguales retorna 0,
   si la primera es menor que la segunda retorna -1,
   en caso contrario retorna 1.

   12/Nov/2004 - Abigail Martínez Rivas
**/
int comparaRGB(RGB color1, RGB color2)
{
	int t;
	
	if( color1.R==color2.R )
		if( color1.G==color2.G )
			if( color1.B==color2.B ) 	t =  0;
			else if( color1.B < color2.B ) 	t = -1;
			else 				t =  1;
		else if( color1.G < color2.G ) 	t = -1;
		else 				t =  1;
	else if( color1.R < color2.R ) 	t = -1;
	else 				t =  1;
	
	return t;
}
