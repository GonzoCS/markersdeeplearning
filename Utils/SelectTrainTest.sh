#!/bin/bash
mkdir "train"
mkdir "test"
x=1

for file in *.png
do
  text="${file/.png/.txt}"
  if [ $(($x % 10)) -eq "0" ]
  then
   echo "Test $file"
    mv $file "test/"
    mv $text "test/"
  else 
    echo "Train $file"
    mv $file "train/"
    mv $text "train/"
  fi
  x=$(( $x + 1))
done
