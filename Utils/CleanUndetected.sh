#!/bin/bash

mkdir "recognized"
mkdir "discarded"
for file in *.png
do
  echo "$file"
  var="${file/.png/.txt}"
  if [ -f "$var" ]; then
    echo "$var exist"
    mv $file "recognized/"
    mv $var "recognized/"
  else 
    echo "$var does not exist, discarding $file"
    mv $file "discarded/"
  fi
done
